import {createStore} from 'redux'

export const actionTypes = {};

const initialState = {};

// REDUCERS
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		default:
			return state
	}
};

export function initializeStore(initialState = initialState) {
	return createStore(reducer, initialState)
}