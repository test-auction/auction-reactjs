const withCSS = require('@zeit/next-css');

let config = withCSS({
	webpack(config, options) {
		config.module.rules.push({
			test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
			use: {
				loader: 'url-loader',
				options: {
					limit: 100000
				}
			}
		});

		return config
	},

	exportPathMap: function () {
		return {
			'/': {page: '/'},
			// '/product/1': {page: '/product', query: {id: "1"}},
			// '/product/2': {page: '/product', query: {id: "2"}},
			// '/product/3': {page: '/product', query: {id: "3"}}
		}
	},
	serverRuntimeConfig: {
		'apiHost': 'https://auction-reactjs.herokuapp.com'
	},

});

module.exports = config;