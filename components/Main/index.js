import React, {Component} from 'react';

import HomePage from '~/components/HomePage';
import PageAbout from '~/components/PageAbout';


import {
	BrowserRouter as Router,
	Link, Route, Switch
} from 'react-router-dom';

class Main extends Component {

	render() {
		return (
			<Router>
				<Switch>
					<Route exact path="/" component={HomePage}/>
					<Route path="/about" component={PageAbout}/>
					<Route
						path="/contact"
						render={() => <h1>Contact Us</h1>}/>
					<Route render={() => <h1>Page not found</h1>}/>
				</Switch>
			</Router>);
	}
}

export default Main;