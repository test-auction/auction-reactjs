import React, {Component} from 'react';

class Copyright extends Component {
	render() {
		let year = new Date().getFullYear();
		return (
			<p className="m-0 text-center text-white">Tegos &copy; ReactJS Auction {year}</p>
		);
	}
}

export default Copyright;