import Link from 'next/link';
import Head from 'next/head';
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

export default ({children, title = ''}) => (
	<div>
		<Head>
			<title>{title}</title>
			<meta charSet='utf-8'/>
			<meta name='viewport' content='initial-scale=1.0, width=device-width'/>
		</Head>

		{children}

		<footer>
			{'I`m here to stay'}
		</footer>
	</div>
)