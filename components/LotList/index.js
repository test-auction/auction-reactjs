import React, {Component} from 'react';

import Lot from '../../components/Lot/Lot';

import './LotList.css';

class LotList extends Component {

	constructor() {
		super();

		this.state = {
			lots: []
		};
	}

	componentDidMount() {
		console.log(this.props);
		console.log(this.state);
		this.setState({lots: this.props.lots});
	}

	render() {
		return (
			<div className="lotList">
				{this.renderLotList()}
			</div>

		);
	}

	renderLotList() {
		let {lots} = this.state;

		if (!Array.isArray(lots) || lots.length < 0) return;

		const size = 10;
		lots = lots.slice(0, 10);

		const length = lots.length;
		if (length) {
			let n = 0;
			return lots.map(lot => {
				n++;
				return (
					<div key={lot.id}>
						<Lot lot={lot}/>
						{(n !== length) ? <hr/> : ''}
					</div>
				);
			});

		}
	}

}


export default LotList;
