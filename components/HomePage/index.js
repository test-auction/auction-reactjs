import React, {Component} from 'react';

import LotList from '~/components/LotList';
import Container from '~/components/Container';
import Header from '~/components/Header';
import Layout from '~/components/Layout';

class HomePage extends Component {

	render() {
		return (
			<Layout>
				<Container>
					<Header/>
					<LotList/>
				</Container>
			</Layout>
		);
	}

}

export default HomePage;