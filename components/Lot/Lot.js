import React, {Component} from 'react';
import $ from 'jquery';

import './Lot.css';

class Lot extends Component {
	constructor(props) {
		super(props);

		let {lot} = this.props;
		this.state = {
			currentPrice: lot.price
		}
	}


	render() {
		let {lot} = this.props;
		return (
			<div className="row" key={lot.id}>
				<div className="col-md-7">
					<a href="#">
						<img className="img-fluid rounded mb-3 mb-md-0" src="http://placehold.it/700x300"
						     alt=""/>
					</a>
				</div>
				<div className="col-md-5">

					<div className="card">
						<div className="card-header">
							<i className="far fa-clock"/>&nbsp;
							<span>3 Days</span>
							&nbsp;
							<span>22 Hrs</span>
							&nbsp;
							<span>53 Min</span>


						</div>
						<div className="card-body">
							<h3 className="card-title">{lot.title}</h3>
							<p className="card-text">
								{lot.description}
							</p>

							<div className="pb-1 row">
								<div className="col-md-12">
									<div>
										<h4>Current price <span className="badge badge-secondary">${lot.price}</span>
										</h4>
										<h5>Bids <span className="badge badge-secondary">5</span></h5>
									</div>

								</div>

							</div>

							<div className="input-group mb-3">
								<input type="number" step="0.01" className="form-control"
								       value={this.state.currentPrice}
								       onChange={this.handlePriceChange}
								       placeholder="Your price"
								       aria-label="Your price" aria-describedby="basic-addon2"/>
								<div className="input-group-append">
									<button onClick={this.handlePlaceBid} className="btn btn-outline-secondary"
									        type="button">Place Bid
									</button>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		)

	}

	handleClick = () => {
		console.log('this is:', this);
	};

	handlePlaceBid = (e) => {
		alert(this.state.currentPrice);
	};

	handlePriceChange = (e) => {
		let newPrice = e.target.value;
		if (!isNaN(newPrice)) {
			this.setState({
				currentPrice: newPrice
			})
		}
	};
}

export default Lot;