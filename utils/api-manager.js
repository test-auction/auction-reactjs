let ApiManger = function (baseUrl = '') {
	let _this = this;
	_this.baseUrl = baseUrl;

	let methods = {
		getLots() {
			return `${baseUrl}/api/lots/`;
		}
	};

	return {...methods};
};

export default ApiManger;