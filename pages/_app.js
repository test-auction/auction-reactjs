import App, {Container} from 'next/app';
import React from 'react';
import withReduxStore from '../utils/with-redux-store';
import {Provider} from 'react-redux';

class AuctionApp extends App {
	render() {
		const {Component, pageProps, reduxStore} = this.props;

		return (
			<Container>
				<Provider store={reduxStore}>
					<div id="main">

						<Component {...pageProps} />

					</div>
				</Provider>
			</Container>
		)
	}
}

export default withReduxStore(AuctionApp);