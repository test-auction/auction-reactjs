import React from 'react';
import {connect} from 'react-redux';
import Head from 'next/head';
import LotList from '../components/LotList';
import getConfig from 'next/config';
import 'isomorphic-unfetch';
import Header from '~/components/Header';
import Layout from '~/components/Layout';
import Container from '~/components/Container';

const {serverRuntimeConfig} = getConfig();
import apiManager from '/utils/api-manager';

const api = new apiManager(serverRuntimeConfig.apiHost);

class Index extends React.Component {
	static async getInitialProps({req}) {
		const userAgent = req ? req.headers['user-agent'] : navigator.userAgent;
		const res = await fetch(api.getLots());
		const lots = await res.json();

		return {userAgent, lots}
	}

	render() {
		return (
			<Layout>
				<Container>
					<Header/>
					<LotList {...this.props}/>
				</Container>
			</Layout>
		)
	}
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(Index);